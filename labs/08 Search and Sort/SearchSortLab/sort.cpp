#include "sort.hpp"

#include <iostream>
using namespace std;

#include "menu.hpp"

void InsertionSort( vector<Animal>& arr )
{
    int size = arr.size();
    int i = 1;
    while ( i < size )
    {
        if ( i % 1000 == 0 ) {
            cout << i << " of " << size << "..." << endl;
        }

        int j = i;
        while ( j > 0 && arr[j-1] > arr[j] )
        {
            Swap( arr[j], arr[j-1] );
            j = j-1;
        }

        i = i + 1;
    }
}

void SelectionSort( vector<Animal>& arr )
{
    int size = arr.size();
    for ( int i = 0; i < size-1; i++ )
    {
        if ( i % 1000 == 0 ) {
            cout << i << " of " << size << "..." << endl;
        }
        int minIndex = i;

        for ( int j = i+1; j < size; j++ )
        {
            if ( arr[j] < arr[minIndex] )
            {
                minIndex = j;
            }
        }

        if ( minIndex != i )
        {
            Swap( arr[i], arr[minIndex] );
        }
    }
}

void Swap( Animal & a, Animal & b )
{
    Animal c = a;
    a = b;
    b = c;
}

// Adapted from https://www.geeksforgeeks.org/merge-sort/
void MergeSort( vector<Animal>& arr, int left, int right )
{
    if ( left < right )
    {
        int mid = ( left + right ) / 2;

        MergeSort( arr, left, mid );
        MergeSort( arr, mid+1, right );

        Merge( arr, left, mid, right );
    }
}

void Merge( vector<Animal>& arr, int left, int mid, int right )
{
    int n1 = mid - left + 1;
    int n2 = right - mid;

    vector<Animal> leftVec;
    vector<Animal> rightVec;

    for ( int i = 0; i < n1; i++ )
    {
        leftVec.push_back( arr[left + i] );
    }

    for ( int j = 0; j < n2; j++ )
    {
        rightVec.push_back( arr[mid + 1 + j] );
    }

    int i = 0;
    int j = 0;

    int k = left;
    while ( i < n1 && j < n2 )
    {
        if ( leftVec[i] <= rightVec[j] )
        {
            arr[k] = leftVec[i];
            i++;
        }
        else
        {
            arr[k] = rightVec[j];
            j++;
        }
        k++;
    }

    while ( i < n1 )
    {
        arr[k] = leftVec[i];
        i++;
        k++;
    }

    while ( j < n2 )
    {
        arr[k] = rightVec[j];
        j++;
        k++;
    }
}

