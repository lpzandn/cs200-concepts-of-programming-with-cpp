#include <iostream>
#include <memory>
using namespace std;

void VanillaPointer()
{
    int someNumber = 10;

    int * ptr = &someNumber;

    *ptr = 100;
    cout << *ptr << endl;
}

void VanillaDynamicVariable()
{
    int * ptr = new int;
    *ptr = 200;
    cout << *ptr << endl;
    delete ptr;
}

void VanillaDynamicArray()
{
    int * ptr = new int[5];

    for ( int i = 0; i < 5; i++ )
    {
        ptr[i] = i * 2;
        cout << ptr[i] << "\t";
    }
    delete [] ptr;
    cout << endl;
}

void UniquePtrDynamicVariable()
{
    unique_ptr<int> ptr;
    *ptr = 10;
    cout << *ptr << endl;
}

void UniquePtrDynamicArray()
{
}

int main()
{
    VanillaPointer();
    VanillaDynamicVariable();
    VanillaDynamicArray();
    UniquePtrDynamicVariable();
}
