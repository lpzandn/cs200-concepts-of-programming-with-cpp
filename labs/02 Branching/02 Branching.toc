\contentsline {chapter}{\numberline {1}Setup}{3}
\contentsline {section}{\numberline {1.1}Turn-in instructions}{3}
\contentsline {section}{\numberline {1.2}Setup}{3}
\contentsline {subsection}{\numberline {1.2.1}Using the starter code}{3}
\contentsline {paragraph}{Project setup:}{3}
\contentsline {subparagraph}{Add existing file in Visual Studio:}{3}
\contentsline {subparagraph}{Add existing file in Code::Blocks:}{4}
\contentsline {subsection}{\numberline {1.2.2}Writing starter code from scratch}{4}
\contentsline {paragraph}{Project setup:}{4}
\contentsline {subsection}{\numberline {1.2.3}About the starter code}{5}
\contentsline {chapter}{\numberline {2}Branching lab 1: Hometown \nobreakspace {}\\(if statements)}{7}
\contentsline {paragraph}{Program overview:}{7}
\contentsline {section}{\numberline {2.1}Reference information}{7}
\contentsline {subsection}{\numberline {2.1.1}String lengths}{7}
\contentsline {subsection}{\numberline {2.1.2}String inputs}{8}
\contentsline {section}{\numberline {2.2}Specifications}{9}
\contentsline {paragraph}{Variables:}{9}
\contentsline {paragraph}{Program flow:}{9}
\contentsline {section}{\numberline {2.3}Testing:}{10}
\contentsline {chapter}{\numberline {3}Branching lab 2: Pass/fail \nobreakspace {}\\(if / else statements)}{11}
\contentsline {paragraph}{Program overview:}{11}
\contentsline {section}{\numberline {3.1}Reference information}{11}
\contentsline {subsection}{\numberline {3.1.1}How do I calculate their grade?}{11}
\contentsline {subsection}{\numberline {3.1.2}What about integers?}{12}
\contentsline {section}{\numberline {3.2}Specifications}{12}
\contentsline {paragraph}{Variables:}{12}
\contentsline {paragraph}{Program flow:}{12}
\contentsline {section}{\numberline {3.3}Testing}{13}
\contentsline {chapter}{\numberline {4}Branching lab 3: Battery charge \nobreakspace {}\\(if / else if statements)}{14}
\contentsline {paragraph}{Program overview:}{14}
\contentsline {section}{\numberline {4.1}Specifications}{15}
\contentsline {paragraph}{Variables:}{15}
\contentsline {paragraph}{Program flow:}{15}
\contentsline {section}{\numberline {4.2}Testing}{16}
\contentsline {chapter}{\numberline {5}Branching lab 4: Input validation \nobreakspace {}\\(logic operators)}{17}
\contentsline {paragraph}{Program overview:}{17}
\contentsline {section}{\numberline {5.1}Reference information}{18}
\contentsline {subsection}{\numberline {5.1.1}What is valid/invalid?}{18}
\contentsline {paragraph}{Way 1: Is the input \relax $\@@underline {\hbox {valid}}\mathsurround \z@ $\relax ? Choose this, or this, or this, or...}{18}
\contentsline {paragraph}{Way 2: Is the input \relax $\@@underline {\hbox {valid}}\mathsurround \z@ $\relax ? Between A and B...}{18}
\contentsline {paragraph}{Way 3: Is the input \relax $\@@underline {\hbox {invalid}}\mathsurround \z@ $\relax ? Didn't choose this or that or that...}{19}
\contentsline {paragraph}{Way 4: Is the input \relax $\@@underline {\hbox {invalid?}}\mathsurround \z@ $\relax Chose a number outside of range...}{19}
\contentsline {paragraph}{What's the best?}{20}
\contentsline {section}{\numberline {5.2}Specifications}{20}
\contentsline {paragraph}{Variables:}{20}
\contentsline {paragraph}{Program flow:}{20}
\contentsline {paragraph}{Example output:}{20}
\contentsline {section}{\numberline {5.3}Testing}{21}
\contentsline {chapter}{\numberline {6}Branching lab 5: Calculator \nobreakspace {}\\(switch statements)}{23}
\contentsline {paragraph}{Program overview:}{23}
\contentsline {section}{\numberline {6.1}Reference information}{24}
\contentsline {subsection}{\numberline {6.1.1}Switch statement:}{24}
\contentsline {subsection}{\numberline {6.1.2}Switch statement flow-through}{24}
\contentsline {subsection}{\numberline {6.1.3}Division by zero?}{25}
\contentsline {section}{\numberline {6.2}Specifications}{25}
\contentsline {paragraph}{Variables:}{25}
\contentsline {paragraph}{Program flow:}{26}
\contentsline {paragraph}{Example output:}{26}
\contentsline {section}{\numberline {6.3}Testing}{27}
