\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {While Loops}
\newcommand{\laTitle}       {CS 200 Lab}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\section*{Lab instructions}

\paragraph{Turn in:}

\begin{itemize}
	\item	Once done, upload the .cpp file(s) that you edited.
	\item	Don't zip the source files.
	\item	Don't zip the entire folder and upload that. I only want source files.
\end{itemize}

\newpage
\section*{While loops lab}

\subsection*{Project setup}

	Some \textbf{starter code} is provided for you for this lab.
	You can download the starter file off the Canvas assignment, or type in the following:

\begin{lstlisting}[style=code]
#include <iostream>
#include <string>
using namespace std;

void Program1()
{
}

void Program2()
{
}

void Program3()
{
}

void Program4()
{
}

void Program5()
{
}

void Program6()
{
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
\end{lstlisting}

	~\\
	If you are downloading the file, make sure to create a new project/solution
	in Visual Studio/Code::Blocks first, and then move the \texttt{branchinglab.cpp}
	file into that folder.

	\paragraph{Add existing file in Visual Studio:}

	\begin{enumerate}
		\item	Right-click on your project file in the \textbf{Solution Explorer}.
		\item	Select \textbf{Add} and choose \textbf{Existing item...}
		\item	Find the file and add it to the project.
	\end{enumerate}

	\paragraph{Add existing file in Code::Blocks:}

	\begin{enumerate}
		\item	Right-click on your project file in the \textbf{Projects} pane.
		\item	Click \textbf{Add files...}
		\item	Find your file and click \textbf{Open}.
		\item	Make sure Debug and Release are both checked and click \textbf{OK}.
		\item	Make sure to save your project (File $>$ Save Project).
	\end{enumerate}

\newpage
\subsection*{About the starter code}

	This starter code uses \textbf{functions}, which we have not covered
	yet in this class, but they are handy for breaking up programs into
	smaller, more managable chunks. \textbf{You do not need to modify main()}
	for this lab. There are five mini-programs you will be implementing,
	and they will go under each \textbf{Program()} function, just like
	how you'd code in \texttt{main()} - except there's no \texttt{return 0;}.

	~\\
	When you run the program, it will ask you which program you want to run.
	You can test out each program this way.

\begin{lstlisting}[style=output]
Run which program? (1-6): 1


(Program 1 runs)

------------------------------------
Run which program? (1-6):
\end{lstlisting}

\newpage
\subsection*{Program 1: Count Up}
	
	Within the \texttt{Program1()} function, write a program that starts
	at 0 and counts up to 20, going up by 1 each time.
	You will need to declare and initialize an integer variable before your while loop,
	and make sure that the integer is incremented inside the while loop.
	
	\begin{hint}{How do I use a while loop?} \footnotesize
		A while loop is similar to an if statement, where it has some condition within its parentheses, but what is different is that the while loop will keep running its internal code while the condition is true, whereas an if statement only runs its internal code once. 
		
\begin{lstlisting}[style=code]
if ( CONDITION_IS_TRUE ) {
    // Do this once
}
    
while ( CONDITION_IS_TRUE ) {
    // Do this while the condition is still true
}
\end{lstlisting}

		A while loop will only stop looping once the condition evaluates to false, or if you use the break; command to escape the loop.
		
		~\\
		\textbf{Warning!} If there is no code INSIDE your while loop that will make its condition evaluate to false, then the loop will run forever. 
	\end{hint}
	
	
	\paragraph{Step-by-step:} ~\\
	
	\begin{enumerate}
		\item	Declare an integer (\texttt{int}) variable named something like \texttt{count}.
		\item	Assign the value 0 to \texttt{count} with an assignment statement. ~\\(\texttt{count = 0;})
		\item	Create a while loop: While \texttt{count} is \texttt{< 21}, or \texttt{<= 20}. Within the loop...
		\begin{enumerate}
			\item	Output the value of \texttt{count} with a \texttt{cout} statement.
			\item	Add 1 to \texttt{count} in one of the following ways: ~\\
					\texttt{count++;} ~\\
					\texttt{count += 1;} ~\\
					\texttt{count = count + 1;}
		\end{enumerate}
	\end{enumerate}

	\paragraph{Build and test!}
	The program output should look like this:

\begin{lstlisting}[style=output]
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
\end{lstlisting}

\newpage
\subsection*{Program 2: Multiply up}

	This program will be similar to program 1, but instead of going from 0 to 20, 
	it will go from 1 to 128, \textit{multiplying} \texttt{count} by 2 each time instead of
	\textit{adding} 1 each time.

	\paragraph{Build and test!}
	The program output should look like this:

\begin{lstlisting}[style=output]
1 2 4 8 16 32 64 128
\end{lstlisting}

	\begin{hint}{Math operations?} ~\\
		\begin{tabular}{l | c | c | p{3cm}}
			\textbf{Operation} 	& \textbf{Long form} 		& \textbf{Short form} 	& \textbf{Special} \\ \hline
			Addition			& \texttt{num = num + 1}	& \texttt{num += 1}		& \texttt{num++}
			\\					&							&						& \texttt{++num}
			\\ \hline
			Subtraction			& \texttt{num = num - 1}	& \texttt{num -= 1}		& \texttt{num--}
			\\					&							&						& \texttt{--num}
			\\ \hline
			Multiplication		& \texttt{num = num * 2}	& \texttt{num *= 2}
			\\ \hline
			Division			& \texttt{num = num / 2}	& \texttt{num /= 2}
		\end{tabular}
		
		~\\~\\
		 \texttt{++} is the \textbf{increment operator}. It will increase a variable by 1,
		 and it can be used inside of another statement. For example, if you write:
		 
\begin{lstlisting}[style=code]
int a = 0;
cout << a++ << endl;	// outputs 0
cout << a << endl;		// outputs 1
\end{lstlisting}
		
		The variable \texttt{a} will have 1 added to it \textbf{after being output}.
		But, if you write:
		 
\begin{lstlisting}[style=code]
int a = 0;
cout << ++a << endl;	// outputs 1
cout << a << endl;		// outputs 1
\end{lstlisting}

		The variable \texttt{a} will have 1 added to it \textbf{prior to being output}.
		
		~\\
		\texttt{--} is the \textbf{decrement operator} and works in the same way,
		except it will decrease a variable by 1.

	\end{hint}

\newpage
\subsection*{Program 3: Number guesser}
	Write a program that will keep asking the user to guess a number, 
	and will only stop once they guess the number.

	Create an integer named \texttt{secretNumber}, and assign it your favorite number. 
	Also create an integer variable named \texttt{playerGuess}. These will go
	\textit{before} the loop, at the start of \texttt{Program3()}.

	Create a \texttt{do...while} loop that will ask the user to guess a number, 
	display a message if it's too high or too low (hint: use an if statement here inside the loop). 
	Your while loop's condition will be "while the player's guess is NOT equal to my secret number!" 

	~\\ The form of a do...while loop is:
\begin{lstlisting}[style=code]
do
{
    // Do this stuff at least once
} while ( CONDITION_IS_TRUE );
\end{lstlisting}

	\paragraph{Step-by-step:} ~\\
	\begin{enumerate}
		\item	Declare variables \texttt{secretNumber} and \texttt{playerGuess}.
		\item	Initialize \texttt{secretNumber} to your favorite number.
		\item	do:
		\begin{enumerate}
			\item	Output the message, ``Guess a number:'' with a \texttt{cout} statement.
			\item	Get the user's input and store it in \texttt{playerGuess} using a \texttt{cin} statement.
			\item	If \texttt{playerGuess} is greater than \texttt{secretNumber}, output the message ``Too high!''
			\item	Otherwise if \texttt{playerGuess} is less than \texttt{secretNumber}, output the message ``Too low!''
			\item	Otherwise if \texttt{playerGuess} is equal to \texttt{secretNumber}, output the message ``That's right!''
		\end{enumerate}
		\item	while \texttt{playerGuess} is not \texttt{secretNumber}.
		\item 	Output the message ``GAME OVER''
	\end{enumerate}
	
	\paragraph{Build and test} ~\\
	The program output will look like:
	
\begin{lstlisting}[style=output]
Guess a number:     5
Too low!

Guess a number:     10
Too high!

Guess a number:     8
That's right!

GAME OVER
\end{lstlisting}

\newpage
\subsection*{Program 4: Input validator}

	In this program, we will write a simple input validator, which will be
	useful in future programs as well. Here, we will display a message to the
	user asking them to enter a number between 1 and 5.
	If they enter something \textit{outside} of that range, then
	an error message will be displayed and they will be required to 
	enter another number - this continues until they finally choose a
	value between 1 and 5.
	
	\begin{itemize}
		\item	Valid input:	Between 1 and 5, inclusive.
		\item	Invalid input:	Less than 1 or greater than 5, exclusive.
	\end{itemize}
	
	Create a variable for the user's input, have them enter a value \textit{before}
	the while loop, and then use a while loop to validate their input and make
	them re-enter a value, if necessary.
	
	\paragraph{Build and test:} ~\\ Example output:
	
\begin{lstlisting}[style=output]
Please enter a number between 1 and 5: 100
Invalid entry, try again: 30
Invalid entry, try again: -2
Invalid entry, try again: 3
Thank you.
\end{lstlisting}

	



\newpage
\subsection*{Program 5: Getting a raise}

	This program will calculate your salary after $n$ amount of years,
	getting some $x$\% raise each year.

	\paragraph{Variables:} ~\\
	
	\begin{center}	
		\begin{tabular}{l l p{6cm}}
			\textbf{Variable name} 			& \textbf{Data type}	& \textbf{Description} \\ \hline
			\texttt{startingWage}			& \texttt{float}		& The user's starting wage \\
			\texttt{percentRaisePerYear}	& \texttt{float}		& The \% wage increase they get per year \\
			\texttt{adjustedWage}			& \texttt{float}		& The adjusted wage, calculated each year \\
			\texttt{yearsWorked}			& \texttt{int}			& The amount of years the user will work \\
			\texttt{yearCounter}			& \texttt{int}			& The year counter used to calculate for each year \\
		\end{tabular}
	\end{center}
	
	 Ask the user to enter data for each of these items, except the \texttt{yearsCounter}, 
	 which will be used later, and \texttt{adjustedWage}. Initialize \texttt{yearsCounter} to 0.

	After the user has entered the \texttt{startingWage},
	set \texttt{adjustedWage}'s initial value to \texttt{startingWage}.

	You'll create a while loop that will iterate \textit{yearsWorked} amount of years, 
	recalculating the new salary for each year. The equation to calculate the latest wage is:
	
	\begin{center} \footnotesize
		\texttt{adjustedWage = (adjustedWage * percentRaisePerYear / 100) + adjustedWage.}
	\end{center}
	
	Make sure to display the wage value each time you calculate it in the loop.

	\begin{hint}{How do I run the loop the right amount of times?}
	You'll create a while loop that will continue while the \texttt{yearCounter} is less than the \texttt{yearsWorked}, 
	and make sure to add 1 to the \texttt{yearCounter} each time. 
	\end{hint}
	
	\newpage
	\paragraph{Build and test} ~\\ Example output:
\begin{lstlisting}[style=output]
What is your starting wage?             100000
What % raise do you get per year?       5
How many years have you worked there?   8

Salary at year 1:   100000
Salary at year 2:   105000
Salary at year 3:   110259
Salary at year 4:   115762
Salary at year 5:   121551
Salary at year 6:   127628
Salary at year 7:   134010
Salary at year 8:   140710
\end{lstlisting}

	\begin{hint}{How do I make these \$ amounts look nicer?}
	There isn't a built-in, easy way to display currency values like ``\$105,000'' in one simple step.
	... So, sorry, don't worry about it for this class. :)
	\end{hint}

\newpage
\subsection*{Program 6: Summation}

	For this program, you will add the sum of all the numbers from 1 to $n$ (the user enters $n$) using a loop.


	\paragraph{Variables:} ~\\
	
	\begin{center}	
		\begin{tabular}{l l l p{6cm}}
			\textbf{Variable} 				& \textbf{Data type}	& \textbf{Initial value} 	& \textbf{Description} \\ \hline
			\texttt{n}						& \texttt{int}			& 	& The ending values $n$ to add up until \\
			\texttt{counter}				& \texttt{int}			& 1	& A number from 1 to $n$ \\
			\texttt{sum}					& \texttt{int}			& 0	& The sum of numbers from 1 to $n$ \\
		\end{tabular}
	\end{center}

	Initialize the \texttt{counter} to 1 and the \texttt{sum} to 0 then ask the user a value for $n$.~\\
	
	Create a loop that loops while \texttt{counter <= n}. Within the loop,
	add the value of \texttt{counter} onto the sum (\texttt{sum += counter}), and increment \texttt{counter} (\texttt{counter++}),
	and display the value of \texttt{sum} each time (using a \texttt{cout}).

	\paragraph{Build and test} ~\\ Example output:
\begin{lstlisting}[style=output]
Enter a value for n: 5

Sum: 1
Sum: 3
Sum: 6
Sum: 10
Sum: 15
\end{lstlisting}

	\begin{hint}{One fewer variable}
		You could also implement this without the \texttt{counter} variable:
		You could loop while $n$ is greater than 0, adding $n$ onto the sum each time,
		and decrementing $n$ each cycle.
		
		$$ 5 + 4 + 3 + 2 + 1 \equiv 1 + 2 + 3 + 4 + 5 $$
	\end{hint}

\end{document}


