#include <iostream>
#include <string>
using namespace std;

void InitializeArray(char grid[5][5])
{
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			grid[i][j] = 'a';
		}
	}
}

void DrawArray(char grid[5][5])
{
	cout << "\t";
	for (int j = 0; j < 5; j++)
	{
		cout << j << "\t";
	}
	cout << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << i << "\t";
		for (int j = 0; j < 5; j++)
		{
			cout << grid[i][j] << "\t";
		}
		cout << endl;
	}
}

int main()
{
	char grid[5][5];
	InitializeArray(grid);

	while (true)
	{
		DrawArray(grid);

		int row, column;

		cout << endl << "Enter a row and column: ";
		cin >> row >> column;

		cout << "Change to symbol: ";
		char newSymbol;
		cin >> newSymbol;

		grid[row][column] = newSymbol;

		cout << endl << endl;
	}

	return 0;
}
