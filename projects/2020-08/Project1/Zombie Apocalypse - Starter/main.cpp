#include <iostream>     // Used for cin and cout
#include <iomanip>      // Used for formatting cout statements
#include <string>       // Used for string data types
#include <cstdlib>      // Used for rand()
#include <ctime>        // time used to seed random number generator
using namespace std;

void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location );

int main()
{
    /******************************************************************* INITIALIZE VARIABLES */
    bool done = false;                      // Flag for if the game is over
    bool successfulAction = false;          // Flag for if the user entered valid input
    int food = 5;                           // Player stat - how much food they have
    int maxHealth = 20;                     // Player stat - their maximum amount of health
    int health = maxHealth;                 // Player stat - their current health amount
    string name = "ME";                     // Player stat - what their name is
    string location = "Overland Park";      // Player stat - what location they're in
    int day = 1;                            // Player stat - how many days it has been

    string dump;                            // A variable to dump input into
    int choice;                             // A variable to dump input into
    int mod;                                // A variable used for calculations later

    srand( time( NULL ) );                  // Seeding the random number generator

    /******************************************************************* GAME START */
    cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
    cout << "Enter your name: ";
    getline( cin, name );

    cout << endl;

    /******************************************************************* GAME LOOP */
    while ( !done )
    {
        /** Start of the round **/
        successfulAction = false;
        DisplayMenu( day, food, health, maxHealth, name, location );
        cout << "CHOICE: ";
        cin >> choice;

        /** Student implements features here **/

        /** End of the round - nothing to update **/
        cout << endl << "Press ENTER to continue...";
        cin.ignore();
        getline( cin, dump );

        cout << "----------------------------------------" << endl;
        cout << "----------------------------------------" << endl;
    }

    /******************************************************************* GAME OVER */
    cout << endl << endl << "You survived the apocalpyse on your own for " << day << " days." << endl;

    return 0;
}

/* You don't need to modify this, it just displays the user's information at the start of each round. */
void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location )
{
    cout << "----------------------------------------" << endl;
    cout << left << setw( 3 ) << "-- " << setw( 35 ) << name << "--" << endl;
    cout << left
        << setw( 6 ) << "-- Day "   << setw( 4 ) << day
        << setw( 6 ) << "Food: "    << setw( 4 ) << food
        << setw( 8 ) << "Health: "  << setw( 2 ) << health << setw( 1 ) << "/" << setw( 2 ) << maxHealth
        << right << setw( 6 ) << "--" << endl;
    cout << left
        << setw( 10 ) << "-- Location: " << setw( 20 ) << location
        << right << setw( 7 ) << "--" << endl;
    cout << "----------------------------------------" << endl;
    cout << "-- 1. Scavenge here                   --" << endl;
    cout << "-- 2. Travel elseware                 --" << endl;
    cout << "----------------------------------------" << endl;
    for ( int i = 0; i < 15; i++ )
    {
        cout << endl;
    }
}
