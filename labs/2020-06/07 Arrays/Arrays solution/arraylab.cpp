#include <iostream>
#include <string>
using namespace std;

void Program1()
{
	const int MAX_COURSES = 6;
	int courseCount = 0;
	string courses[MAX_COURSES];

	cout << "COURSE LIST" << endl;
	cout << endl;
	for ( int i = 0; i < MAX_COURSES; i++ )
	{
		cout << "Enter course " << (i+1) << " or type STOP: ";
		string input;
		cin >> input;

		if ( input == "STOP" )
		{
			break;
		}

		courses[courseCount] = input;
		courseCount++;
	}

	cout << endl;
	cout << "Your classes:" << endl;
	for ( int i = 0; i < courseCount; i++ )
	{
		cout << courses[i] << "\t";
	}

	cout << endl << endl;
}

void Program2()
{
}

void Program3()
{
}

int main()
{
    bool done = false;

    while ( !done )
    {
        int choice;
        cout << "0. QUIT" << endl;
        cout << "1. Program 1" << endl;
        cout << "2. Program 2" << endl;
        cout << "3. Program 3" << endl;
        cout << endl << ">> ";
        cin >> choice;

        switch( choice )
        {
            case 0: done = true; break;
            case 1: Program1(); break;
            case 2: Program2(); break;
            case 3: Program3(); break;
        }
    }

    return 0;
}
