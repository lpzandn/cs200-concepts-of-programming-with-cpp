#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
    vector<string> sentence = { "Hi", "there", "how", "are", "you", "?" };

    for ( string item : sentence )
    {
        cout << item << "\t";
    }

    return 0;
}
