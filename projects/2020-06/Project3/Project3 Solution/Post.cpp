#include "Post.hpp"

#include "Menu.hpp"

#include <iostream>
using namespace std;

Post::Post()
{
}

Post::Post( string authorName, string text )
{
    Setup( authorName, text );
}

void Post::Setup( string authorName, string text )
{
    m_author = authorName;
    m_text = text;
}

void Post::Display() const
{
    Menu::DrawHorizontalBar( 80, '*' );
    cout << m_text << endl;
    cout << "-- " << m_author << endl;
    Menu::DrawHorizontalBar( 80, '*' );
}

string Post::GetText() const
{
    return m_text;
}

string Post::GetAuthor() const
{
    return m_author;
}
