// Practicing with Variables, Data Types, and Math operators

#include <iostream>     // cout - console output and cin - console input
using namespace std;

int main()
{
    // A = 4, B = 3, C = 2, D = 1, F = 0

    float grade1, grade2, grade3, grade4;
    float gpa;

    cout << "GPA CALCULATION PROGRAM" << endl;

    cout << "Please enter grade 1 (0 - 4): ";
    cin >> grade1;

    cout << "Please enter grade 2 (0 - 4): ";
    cin >> grade2;

    cout << "Please enter grade 3 (0 - 4): ";
    cin >> grade3;

    cout << "Please enter grade 4 (0 - 4): ";
    cin >> grade4;

    // Calculating the GPA
    gpa = ( grade1 + grade2 + grade3 + grade4 ) / 4;

    cout << "Your GPA is: " << gpa << endl;
    cout << "Good work?" << endl;


    return 0;   // return 0, nothing bad happened.
}
