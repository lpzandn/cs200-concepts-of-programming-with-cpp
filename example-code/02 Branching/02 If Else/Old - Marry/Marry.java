import java.util.Scanner;

public class Marry
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );
        
        System.out.println( "Will you marry me?" );
        System.out.println( "\t 1. Yes" );
        System.out.println( "\t 2. No" );

        int choice = input.nextInt();

        if ( choice == 1 )
        {
            System.out.println( ":D" );
        }
        else
        {
            System.out.println( ":(" );
        }
    }
}
