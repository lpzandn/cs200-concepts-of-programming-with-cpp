public class WhileVsFor
{
    public static void main( String args[] )
    {
        int i = 20;

        System.out.println( "WHILE LOOP..." );

        while ( i > 0 )
        {
            System.out.print( i + "\t" );
            i--;
        }


        System.out.println();
        System.out.println( "FOR LOOP..." );

        for ( i = 20; i > 0; i-- )
        {
            System.out.print( i + "\t" );
        }
    }
}
