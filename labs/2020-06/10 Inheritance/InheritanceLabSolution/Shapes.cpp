#include "Shapes.hpp"


/* RECTANGLE ***********************************************************/
/***********************************************************************/

void Rectangle::Setup( float width, float height, string unit )
{
    Shape::Setup( unit );
    m_width = width;
    m_height = height;
}

float Rectangle::CalculateArea()
{
    return m_width * m_height;
}

void Rectangle::DisplayArea()
{
    cout << CalculateArea() << "\t" << m_unit << "^2" << endl;
}

/* CIRCLE **************************************************************/
/***********************************************************************/

void Circle::Setup( float radius, string unit )
{
    Shape::Setup( unit );
    m_radius = radius;
}

float Circle::CalculateArea()
{
    return 3.14 * m_radius * m_radius;
}

void Circle::DisplayArea()
{
    cout << CalculateArea() << "\t" << m_unit << "^2" << endl;
}

/* SHAPE (parent class) FUNCTIONS **************************************/
/***********************************************************************/

void Shape::Setup( string unit )
{
    m_unit = unit;
}

void Shape::DisplayArea()
{
    cout << " " << m_unit << "^2";
}

float Shape::CalculateArea()
{
    return 0;
}
