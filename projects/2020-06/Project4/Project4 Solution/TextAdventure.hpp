#ifndef _TEXT_ADVENTURE_HPP
#define _TEXT_ADVENTURE_HPP

#include "Room.hpp"

class TextAdventure
{
    public:
    TextAdventure();
    ~TextAdventure();
    void Run();

    private:
    bool m_isDone;
    Room * m_rooms;             // dynamic array
    int m_totalRooms;
    Room * m_ptrCurrentRoom;    // pointer to room

    void LoadGameData();

    void AllocateSpace( int size );
    void DeallocateSpace();

    int GetIndexOfRoomWithName( string name );
    string GetUserCommand();
    void TryToMove( string direction );
};

#endif
