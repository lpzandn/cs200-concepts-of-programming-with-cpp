import java.util.Scanner;

public class EmployeeProgram
{
    static Scanner input = new Scanner( System.in );

    // Departments
    static Department dptSW = new Department( "Software Development" );
    static Department dptQA = new Department( "Quality Assurance" );
    static Department dptHR = new Department( "Human Resources" );
    
    // Employees
    static Employee employee1 = new Employee();
    static Employee employee2 = new Employee();
    static Employee employee3 = new Employee();
    static Employee employee4 = new Employee();
    static Employee employee5 = new Employee();
    
    public static void main()
    {
        SetupEmployees();
        DisplayEmployees();
    }
    
    public static void SetupEmployees()
    {
        employee1.Setup( "Wawnee Silliams", 200000, dptQA );
        employee2.Setup( "Ichard Rallen", 100000, dptSW );
        employee3.Setup( "Josh Bookin",    40000,  dptHR );
        
        employee4.Setup( "Pat Piong",    60000, dptSW );
        employee5.Setup( "Craig Bears",   80000, dptHR );
    }
    
    public static void DisplayEmployees()
    {
        employee1.Display();
        employee2.Display();
        employee3.Display();
        employee4.Display();
        employee5.Display();
    }
}
