import java.util.Scanner;

public class TacoMenu
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );

        System.out.println( "Welcome to Taco Bell..." );
        System.out.println( "1. Nacho Cheese Doritos Locos Taco Supreme" );
        System.out.println( "2. XXL Grilled Stuft Burrito" );
        System.out.println( "3. Crunchwrap Supreme" );
        System.out.println( "4. Chicken Quesadilla" );

        int choice = input.nextInt();

        int calories = 0;
        double price = 0;

        switch( choice )
        {
                case 1:
                calories = 190;
                price = 1.99;
                break;
                
                case 2:
                calories = 870;
                price = 4.39;
                break;

                case 3:
                calories = 530;
                price = 3.49;
                break;

                case 4:
                calories = 510;
                price = 3.49;
                break;
        }

        System.out.println();
        System.out.println( "Calories: " + calories );
        System.out.println( "Price: " + price );
    }
}
